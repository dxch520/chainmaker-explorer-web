/*
 *
 *  *
 *  *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  *  SPDX-License-Identifier: Apache-2.0
 *  *
 *
 */

import { PagingWithChainId } from '.';

export interface GetBlockListParam extends PagingWithChainId {
  /**
   * 区块hash值
   */
  Block?: string;
  /**
   * 开始时间
   */
  StartTime?: number;
  /**
   * 结束时间
   */
  EndTime?: number;
}
export interface BlockItem {
  /**
   * 区块逻辑ID
   */
  Id: number;
  /**
   * 区块高度
   */
  BlockHeight: number;
  /**
   * 区块hash
   */
  BlockHash: string;
  /**
   * 交易数量
   */
  TxCount: number;
  /**
   * 提案节点
   */
  ProposalNodeId: string;
  /**
   * 时间戳
   */
  Timestamp: number;
}

export interface BlockInfo {
  /**
   * 区块hash
   */
  BlockHash: string;
  /**
   * 前一个区块hash
   */
  PreBlockHash: string;
  /**
   * 提案节点
   */
  ProposalNodeId: string;
  /**
   * 提案节点地址
   */
  ProposalNodeAddr: string;
  /**
   * 交易merkle哈希
   */
  TxRootHash: string;
  /**
   * 交易merkle哈希
   */
  RwSetHash: string;
  /**
   * 交易数量
   */
  TxCount: number;
  /**
   * 区块hash
   */
  BlockHeight: number;
  /**
   * 提案组织id
   */
  OrgId: string;
  /**
   * Dag特征摘要
   */
  Dag: string;
  /**
   * 时间戳
   */
  Timestamp: number;
  /**
   * 节点地址
   */
  // UserAddr: string;
}
