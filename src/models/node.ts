/*
 *
 *  *
 *  *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  *  SPDX-License-Identifier: Apache-2.0
 *  *
 *
 */

import { PagingWithChainId } from './common';

export interface GetNodeListParam extends PagingWithChainId {
  /**
   * 节点名称
   */
  NodeName: string;
  /**
   * 组织id
   */
  OrgId: string;
}

export interface NodeItem {
  /**
   * 节点逻辑id
   */
  Id: number;
  /**
   * 节点Id
   */
  NodeId: string;
  /**
   * 节点名称
   */
  NodeName: string;
  /**
   * 节点地址
   */
  NodeAddress: string;
  /**
   * 用户角色
   */
  Role: string;
  /**
   * 组织名
   */
  OrgId: string;
  /**
   * 区块高度
   */
  BlockHeight: number;
  /**
   * 创建时间
   */
  Timestamp: number;
}
