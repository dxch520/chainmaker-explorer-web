/*
 *
 *  *
 *  *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  *  SPDX-License-Identifier: Apache-2.0
 *  *
 *
 */

export interface Paging {
  /**
   * 分页偏移量
   */
  Offset?: number;
  /**
   * 当前查询的数量，默认为每页显示数量
   */
  Limit: number;
}

export interface PagingWithChainId extends Paging {
  /**
   * 链id
   */
  ChainId: string | undefined;
}

export interface ParamWithChainId {
  /**
   * 链id
   */
  ChainId: string | undefined;
}

export type ValueOf<T> = T[keyof T];
