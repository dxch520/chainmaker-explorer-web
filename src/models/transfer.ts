import { PagingWithChainId } from './common';

export interface GetTransferListParam extends PagingWithChainId {
  /**
   * 合约名称
   */
  ContractName: string;
  TokenId?: string;
}

export interface TransferItem {
  Id: number;
  /**
   * 上链时间
   */
  BlockTime: number;
  /**
   * 交易id
   */
  TxId: string;
  /**
   * 合约方法
   */
  ContractMethod: string;
  /**
   * 来源
   */
  From: string;
  /**
   * 转向的地址
   */
  To: string;
  /**
   * TokenId
   */
  TokenId: string;
  /**
   * 交易状态
   */
  Status: string;
}

export interface GetNFTDetailParam {
  /**
   * 链id
   */
  ChainId: string;
  /**
   * tokenId
   */
  TokenId: string;
  /**
   * 合约名称
   */
  ContractName: string;
}

export interface NFTInfo {
  /**
   * Id
   */
  Id: number;
  /**
   * tokenId
   */
  ChainId: string;
  /**
   * tokenId
   */
  TokenId: string;
  /**
   * 创建时间
   */
  CreateTime: number;
  /**
   * 作品名称
   */
  Name: string;
  /**
   * 作者名
   */
  Author: string;
  /**
   * 作品url
   */
  Url: string;
  /**
   * 作品hash
   */
  Hash: string;
  /**
   * 描述
   */
  Describe: string;
  /**
   * 当前持有者
   */
  Owner: string;
}
