/*
 *
 *  *
 *  *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  *  SPDX-License-Identifier: Apache-2.0
 *  *
 *
 */

import { ParamWithChainId } from './common';

export type GetDecimalParam = ParamWithChainId;
export type GetTransactionNumByTimeParam = ParamWithChainId;
export interface ChainInfo {
  /**
   * 区块高度
   */
  BlockHeight: number;
  /**
   * 今日交易数量
   */
  TransactionNum: number;
  /**
   * 合约数量
   */
  ContractNum: number;
  /**
   * 组织数量
   */
  OrgNum: number;
  /**
   * 节点数量
   */
  NodeNum: number;
  /**
   * 用户数量
   */
  UserNum: number;
}

export interface TransactionNumByTime {
  /**
   * 交易数量
   */
  TxNum: number;
  /**
   * 时间戳
   */
  Timestamp: number;
}
export interface SearchParam {
  /**
   * 链id
   */
  ChainId?: string;
  /**
   * 查询输入值
   */
  Id: string;
}

export interface SearchInfo {
  /**
   * 链id
   */
  ChainId: string;
  /**
   * 返回数据类型
   */
  Type: number;
  /**
   * 查询详情逻辑ID
   */
  Id: number;
  /**
   * 不同类型的不同标识
   */
  Data: string;
}
