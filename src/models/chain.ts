/*
 *
 *  *
 *  *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  *  SPDX-License-Identifier: Apache-2.0
 *  *
 *
 */

import { Paging, ParamWithChainId } from './common';
import { ChainStatusVales } from '@src/utils/enums';

export type CancelSubscribeParam = ParamWithChainId;
export type DeleteSubscribeParam = ParamWithChainId;
export interface ChainListParam extends Paging {
  /**
   * 根据ChainId搜索
   */
  ChainId?: string;
}

// 链列表中的链
export interface ChainItem {
  /**
   * 账户模式
   */
  AuthType: string;
  /**
   * 链逻辑ID
   */
  Id: number;
  /**
   * 链ID
   */
  ChainId: string;
  /**
   * 链版本
   */
  ChainVersion: string;
  /**
   * 共识算法
   */
  Consensus: string;
  /**
   * 时间戳
   */
  Timestamp: number;
  /**
   * 状态
   */
  Status: ChainStatusVales;
}

/**
 * 监听链的参数
 */
export interface Subscribe {
  /**
   * 链id
   */
  ChainId?: string;
  /**
   * 账户模式
   */
  AuthType: string;
  /**
   * 组织Id
   */
  OrgId?: string;
  /**
   * 用户证书
   */
  UserCert?: string;
  /**
   * 用户key
   */
  UserKey: string;
  /**
   * 密码算法
   */
  HashType?: string | number;
  /**
   * 节点列表
   */
  NodeList:{
    /**
     * 地址
     */
    Addr: string;
    /**
     * 是否开启 tls
     */
    Tls?: boolean;
    /**
     * node节点的CA证书
     */
    OrgCA?: string;
    /**
     * tls连接使用的域名
     */
    TLSHostName?: string;
  }[]
}
