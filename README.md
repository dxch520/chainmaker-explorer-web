node版本 14.18.1
## 命令列表
### `npm run start`

启动本地开发
浏览器打开 [http://localhost:3000](http://localhost:3000) 看效果

### `npm run test`

执行测试
通过 [running tests](https://facebook.github.io/create-react-app/docs/running-tests) 查看更多信息
### `yarn run build`

构建项目到build目录

## 分支说明

分支名 dev/public_{version}  对应社区版浏览器 version版本。
分支名 dev/opennet_{version} 对应开发测试网络浏览器 version版本。

## tag标签说明

tag名称 v{version}   对应社区版浏览器 version版本。
tag名称 opennet/v{version}   对应开发测试网络浏览器 version版本。

## 文档列表

产品文档 [https://www.tapd.cn/47654106/prong/stories/view/1147654106001008373?url_cache_key=465ec1eedc257e5407834ab625b177e0&action_entry_type=story_tree_list](https://www.tapd.cn/47654106/prong/stories/view/1147654106001008373?url_cache_key=465ec1eedc257e5407834ab625b177e0&action_entry_type=story_tree_list)
原型 [https://modao.cc/app/211f90b8cf4bcdb65aff1e327c1cd96df5a65742#screen=skw229j468cxa7p](https://modao.cc/app/211f90b8cf4bcdb65aff1e327c1cd96df5a65742#screen=skw229j468cxa7p)
蓝湖ui文档 [https://lanhuapp.com/web/#/item/project/stage?tid=e1f9669d-9325-4730-88a1-77e3430f4981&pid=d89cbf25-a0f7-45bf-90ee-0e4779d2887d](https://lanhuapp.com/web/#/item/project/stage?tid=e1f9669d-9325-4730-88a1-77e3430f4981&pid=d89cbf25-a0f7-45bf-90ee-0e4779d2887d)


## 目录

    |--  README.md # 服务功能介绍文档
    |--  config-overrides.js # webpack扩展配置
    |--  src  # 项目代码根目录
        |--  assets #静态资源
        |--  components #公共组件资源
            |-- [name] #存放[name]相关公共组件资源
        |-- models #类型定义文件
            |-- [name] #[name]相关类型接口定义文件
        |-- routes #页面ts组件文件
            |-- addChain #首次进入项目时需要添加一个链
            |-- main #已有链的情况下，默认进入链首页
                |-- [page] #[page]相关页面相关组件
        |-- utils #功能模块代码
            |-- apis #接口文件
            |-- validata #校验格式文件
            |-- mock.ts #mock数据
        |-- #App.tsx #网站入口文件
        |-- #Index.tsx #初始化react文件

## 引用顺序图 原则尽量不出现循环引用

```mermaid
graph TD
    功能模块代码 --> 公共组件
    功能模块代码 --> Page组件
    功能模块代码 --> Page相关组件
    Page相关组件 --> Page组件
    公共组件 --> Page相关组件
    公共组件 --> Page组件
```

